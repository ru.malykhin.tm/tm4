package ru.malykhin.tm.controller;

import org.junit.Assert;
import org.junit.Test;
import ru.malykhin.tm.repository.ProjectRepository;
import ru.malykhin.tm.repository.TaskRepository;
import ru.malykhin.tm.service.ProjectService;
import ru.malykhin.tm.service.TaskService;

public class SystemControllerTest {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

    @Test
    public void testSystem() {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
        systemController.saveAll(projectController,taskController);
        Assert.assertEquals(2,projectRepository.findAll().size());
        Assert.assertEquals(2,taskRepository.findAll().size());
        taskRepository.clear();
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        systemController.loadAll(projectController,taskController);
        Assert.assertEquals(2,projectRepository.findAll().size());
        Assert.assertEquals(2,taskRepository.findAll().size());
    }
}
