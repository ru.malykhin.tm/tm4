package ru.malykhin.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.malykhin.tm.repository.TaskRepository;

public class TaskServiceTest {

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    @Test
    public void testSrv() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.create("test");
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskRepository.findByName("test"));
        Assert.assertNotNull(taskRepository.removeByName("test"));
        Assert.assertNull(taskRepository.removeByName("test"));
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.create("test2");
        taskRepository.saveTask();
        taskRepository.removeByName("test2");
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.loadTask();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskRepository.findByName("test2"));
        taskRepository.create("test3");
        Assert.assertEquals(2,taskRepository.findAll().size());




    }

}
