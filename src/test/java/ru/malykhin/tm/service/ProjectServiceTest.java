package ru.malykhin.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.malykhin.tm.repository.ProjectRepository;

public class ProjectServiceTest {
    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService ProjectService = new ProjectService(projectRepository);

    @Test
    public void testSrv() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.create("test");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(projectRepository.findByName("test"));
        Assert.assertNotNull(projectRepository.removeByName("test"));
        Assert.assertNull(projectRepository.removeByName("test"));
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }
}
